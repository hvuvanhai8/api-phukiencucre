<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

// Route::group([
//     'middleware' => 'checkAuth'
// ], function () {
//     Route::get('products','Api\ProductsController@index');
//     Route::get('products/{id}','Api\ProductsController@show');
//     Route::post('products','Api\ProductsController@store');
//     Route::put('products/{id}','Api\ProductsController@update');
// });

Route::group(['middleware'=>'CheckAuth'],function(){
   
});

Route::get('products','Api\ProductsController@index');
Route::get('products/{id}','Api\ProductsController@show');
Route::post('products','Api\ProductsController@store');
Route::put('products/{id}','Api\ProductsController@update');