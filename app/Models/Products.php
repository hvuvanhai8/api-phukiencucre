<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = 'products';

    static $columns_alias = [
        'id' => 'id',
        'pro_code' => 'code',
        'pro_model' => 'model',
        'pro_name' => 'name',
        'pro_slug' => 'slug',
        'pro_quantity' => 'quantity',
        'pro_image' => 'image',
        'pro_origin' => 'origin',
        'pro_capacity' => 'capacity',
        'pro_read_random' => 'read_random',
        'pro_read' => 'read',
        'pro_write' => 'write',
        'pro_brand' => 'brand',
        'pro_guarantee' => 'guarantee',
        'pro_stability' => 'stability',
        'pro_standard_connect' => 'standard_connect',
        'pro_bus' => 'bus',
        'pro_type' => 'type',
        'pro_color' => 'color',
        'pro_sensitive' => 'sensitive',
        'pro_weight' => 'weight',
        'pro_view' => 'view',
        'pro_brightness' => 'brightness',
        'pro_language' => 'language',
        'pro_size' => 'size',
        'pro_cord' => 'cord',
        'pro_switch' => 'switch',
        'pro_battery' => 'battery',
        'pro_wattage' => 'wattage',
        'pro_process_speed' => 'process_speed',
        'pro_cpu_core' => 'cpu_core',
        'pro_teaser' => 'teaser',
        'pro_description' => 'description',
        'pro_price' => 'price',
        'pro_price_discount' => 'price_discount',
        'pro_active' => 'active',
        'pro_rating' => 'rating',
        'pro_hot' => 'hot'
    ];

    static $table_schema = [
        'id' => [
            'type' => 'int',
            'column'=>'id'
        ],
        'hot' => [
            'type' => 'int',
            'column'=>'pro_hot'
        ]
    ];

    public function pro_category()
    {
        return $this->belongsTo(Category::class, 'pro_cat_id', 'id');
    }

    static function alias($fields = null)
    {
        $newFields = [];
        if ($fields == "*" || empty($fields)) {
            foreach (self::$columns_alias as $key=>$alias) {
                if(array_search($alias,self::$columns_alias)){
                    $newFields[] = $key." AS ".$alias;
                }
            }
        }else{
            $fields = explode(",", $fields);
            foreach ($fields as $alias) {
                $field = array_search($alias,self::$columns_alias);
                if ($field) $newFields[] = $field . " AS " . $alias;
            }
        }
        return $newFields;
    }
}
