<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   
    public $page = 1;
    public $fields = '*';
    public $where = null;
    public $where_in = null;
    public $where_between = null;
    public $limit = 30;

    protected $statusCodes = [
		'done' => 200,
		'created' => 201,
		'removed' => 204,
		'not_valid' => 400,
		'not_found' => 404,
		'conflict' => 409,
		'permissions' => 401
	];

    function __construct(Request $request){

        // $this->middleware('auth:api', ['except' => ['login']]);
        // dd($request->where);

        if ($request->page) {
            $page = (int)$request->page;
            if ($page < 1 || $page > 100000) {
                $this->page = 1;
            } else {
                $this->page = $page;
            }
            $this->page = (int)$request->page;
        }
        if ($request->fields) $this->fields = urldecode($request->fields);
        if ($request->where) $this->where = urldecode($request->where);
        if ($request->where_in) $this->where_in = urldecode($request->where_in);
        if ($request->where_between) $this->where_between = urldecode($request->where_between);
        if ($request->limit) {
            $limit = (int)$request->limit;
            if ($limit < 1 || $limit > 100) {
                $this->limit = 30;
            } else {
                $this->limit = (int)$request->limit;
            }
        }
    }

    function setResponse($status, $data = null, $message_error = '')
    {
        if ($status != 200) {
            return [
                'status_code' => $status,
                'message' => $message_error
            ];
        } else {
            $data_return = ['status_code' => 200, 'data' => $data];
            return $data_return;
        }
    }

    function createResponse($data, $status_code = 200, $header = [])
    {
        if(!is_numeric($status_code)){
            $status_code = $this->statusCodes[$status_code];
        }
        $response = ['status' => $status_code, 'data' => $data];
        return response($response, $status_code, $header);
    }

    protected function respond($status, $data = [])
    {
    	return response()->json(["data" => $data,"status" => $this->statusCodes[$status]], $this->statusCodes[$status]);
    }

    protected function optimizeVali($validator){
        $arrReturn = [];
        $data = $validator->messages();
        if(is_object($data)) $data = $data->toArray();
        foreach ($data as $key => $message){
            $arrReturn[$key] = isset($message[0]) ? $message[0] : $message;
        }
        return $arrReturn;
    }
}
