<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Products;
use Illuminate\Support\Str;
use Validator;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Products::select(Products::alias($this->fields));
        $offset = $this->page * $this->limit - $this->limit;
        if ($this->where != null) $products = whereQueryBuilder($this->where, $products, Products::$table_schema, 'Products');
        if ($this->where_in != null) $products = whereInRawQueryBuilder($this->where_in, $products, Products::$table_schema, 'Products');
        $products = $products->offset($offset)->orderBy('id','desc')->limit($this->limit)->get();
        
        foreach($products as $key => $pro){
            $pro->image = explode("|",$pro->image);
            $pro->image = 'http://phukiencucre.local/web/images/product/'.$pro->image[0];
        }
        // dd($products);
        $data = [
            'data' => $products,
            'current_page' => $this->page,
            'per_page' => $this->limit,
        ];
        return $this->createResponse($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'pro_name' => 'required|min:5|max:255',
            'pro_model' => 'required|min:5|max:255'
        ],
        [
            'required' => ':attribute Không được để trống',
            'min' => ':attribute Không được nhỏ hơn :min',
            'max' => ':attribute Không được lớn hơn :max',
            'unique' => ':attribute Không được trùng lặp'
        ],
        [
            'pro_name' => 'Tên sản phẩm',
            'pro_image' => 'Ảnh sản phẩm'
        ]);
        if ($validator->fails()) return $this->respond('not_valid',$this->optimizeVali($validator));

        $product = new Products;
        $product->pro_cat_id = $request->pro_cat_id;
        $product->pro_code = $request->pro_code;
        $product->pro_model = $request->pro_model;
        $product->pro_name = $request->pro_name;
        $product->pro_slug = Str::slug($request->pro_name,'-');
        $product->pro_quantity = $request->pro_quantity;
        $product->pro_origin = $request->pro_origin;
        $product->pro_capacity = $request->pro_capacity;
        $product->pro_read_random = $request->pro_read_random;
        $product->pro_read = $request->pro_read;
        $product->pro_write = $request->pro_write;
        $product->pro_brand = $request->pro_brand;
        $product->pro_guarantee = $request->pro_guarantee;
        $product->pro_stability = $request->pro_stability;
        $product->pro_standard_connect = $request->pro_standard_connect;
        $product->pro_bus = $request->pro_bus;
        $product->pro_type = $request->pro_type;
        $product->pro_color = $request->pro_color;
        $product->pro_sensitive = $request->pro_sensitive;
        $product->pro_weight = $request->pro_weight;
        $product->pro_view = $request->pro_view;
        $product->pro_brightness = $request->pro_brightness;
        $product->pro_language = $request->pro_language;
        $product->pro_size = $request->pro_size;
        $product->pro_cord = $request->pro_cord;
        $product->pro_switch = $request->pro_switch;
        $product->pro_battery = $request->pro_battery;
        $product->pro_wattage = $request->pro_wattage;
        $product->pro_process_speed = $request->pro_process_speed;
        $product->pro_cpu_core = $request->pro_cpu_core;
        $product->pro_teaser = $request->pro_teaser;
        $product->pro_description = $request->pro_description;
        $product->pro_price = $request->pro_price;
        $product->pro_price_discount = $request->pro_price_discount;
        $product->pro_active = $request->pro_active;
        $product->pro_rating = $request->pro_rating;
        $product->pro_hot = $request->pro_hot;
        $product->pro_price = $request->pro_price;
        $product->pro_price_discount = $request->pro_price_discount;

        $images=[];
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=time().'.'.$file->getClientOriginalName();
                $destinationPath = public_path('/web/images/product');
                $file->move($destinationPath,$name);
                $images[]=$name;
            }
        }
        $product->pro_image = implode("|",$images);

        if($product->save()){
            return $this->createResponse('Thêm mới sản phẩm thành công',200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Products::select(Products::alias($this->fields))->where('id',$id)->first();
        if(empty($product)) return $this->setResponse(404,null,'Not found id');
        $data = [
            'product'=>$product,
        ];
        return $this->createResponse($data,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $product = Products::find($id);
        if(empty($product)) return $this->setResponse(404,null,'sản phẩm không tồn tại');

        $validator = Validator::make($request->all(), [
            'pro_name' => 'required|min:5|max:255',
            'pro_model' => 'required|min:5|max:255'
        ],
        [
            'required' => ':attribute Không được để trống',
            'min' => ':attribute Không được nhỏ hơn :min',
            'max' => ':attribute Không được lớn hơn :max',
            'unique' => ':attribute Không được trùng lặp'
        ],
        [
            'pro_name' => 'Tên sản phẩm',
            'pro_image' => 'Ảnh sản phẩm'
        ]);
        if ($validator->fails()) return $this->respond('not_valid',$this->optimizeVali($validator));

        $product->pro_cat_id = $request->pro_cat_id;
        $product->pro_code = $request->pro_code;
        $product->pro_model = $request->pro_model;
        $product->pro_name = $request->pro_name;
        $product->pro_slug = Str::slug($request->pro_name,'-');
        $product->pro_quantity = $request->pro_quantity;
        $product->pro_origin = $request->pro_origin;
        $product->pro_capacity = $request->pro_capacity;
        $product->pro_read_random = $request->pro_read_random;
        $product->pro_read = $request->pro_read;
        $product->pro_write = $request->pro_write;
        $product->pro_brand = $request->pro_brand;
        $product->pro_guarantee = $request->pro_guarantee;
        $product->pro_stability = $request->pro_stability;
        $product->pro_standard_connect = $request->pro_standard_connect;
        $product->pro_bus = $request->pro_bus;
        $product->pro_type = $request->pro_type;
        $product->pro_color = $request->pro_color;
        $product->pro_sensitive = $request->pro_sensitive;
        $product->pro_weight = $request->pro_weight;
        $product->pro_view = $request->pro_view;
        $product->pro_brightness = $request->pro_brightness;
        $product->pro_language = $request->pro_language;
        $product->pro_size = $request->pro_size;
        $product->pro_cord = $request->pro_cord;
        $product->pro_switch = $request->pro_switch;
        $product->pro_battery = $request->pro_battery;
        $product->pro_wattage = $request->pro_wattage;
        $product->pro_process_speed = $request->pro_process_speed;
        $product->pro_cpu_core = $request->pro_cpu_core;
        $product->pro_teaser = $request->pro_teaser;
        $product->pro_description = $request->pro_description;
        $product->pro_price = $request->pro_price;
        $product->pro_price_discount = $request->pro_price_discount;
        $product->pro_active = $request->pro_active;
        $product->pro_rating = $request->pro_rating;
        $product->pro_hot = $request->pro_hot;
        $product->pro_price = $request->pro_price;
        $product->pro_price_discount = $request->pro_price_discount;

        $images=[];
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=time().'.'.$file->getClientOriginalName();
                $destinationPath = public_path('/web/images/product');
                $file->move($destinationPath,$name);
                $images[]=$name;
            }
            $product->pro_image = implode("|",$images);
        }

        if($product->save()){
            return $this->createResponse('Cập nhật sản phẩm thành công',200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
