<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if(!empty($token)){
            return $next($request);
        }
        return response()->json(["status"=>401,"message"=>"Thông tin đăng nhập hết hạn!",'error'=>'middleware'],401);
    }
}
