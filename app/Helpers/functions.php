<?php
function abc()
{
    echo 1223;
}

function getDateFromTo($fd, $td)
{
    $days = (strtotime($td) - strtotime($fd)) / (60 * 60 * 24);
    $firstDay = strtotime($fd);
    $result = [];
    for ($i = 0; $i <= $days; $i++) {
        $b = $firstDay + 86400 * $i;
        $b = date('Y-m-d', $b);
        $result[] = $b;
    }
    return $result;
}


function whereQueryBuilder($where, $model, $table_schema, $connection)
{
    // dd('table_schema', $table_schema);
    $databases_config = $table_schema;
    $options = explode(',', $where);
    foreach ($options as $value) {
        $value = explode(" ", $value);
        if (isset($value[1]) && key_exists($value[0], $databases_config)) {
            $data_column = $databases_config[$value[0]];
            $type = $data_column['type'];
            switch ($type) {
                case 'int':
                    $value[1] = (int)$value[1];
                    $model = $model->where($data_column['column'], $value[1]);
                    break;
                case 'double':
                    $value[1] = (double)$value[1];
                    $model = $model->where($data_column['column'], $value[1]);
                    break;
                case 'string':
                    $value[1] = trim($value[1]);
                    if (in_array($value[1], $data_column['values'])) {
                        $model = $model->where($data_column['column'], $value[1]);
                    }
                    break;
            }
        }

    }

    return $model;
}

function whereQueryBuilderV2($where, $model, $connection, $table)
{
    $databases_config = config('alias_database_v2');
    if (isset($databases_config[$connection][$table])) {
        $databases_config = $databases_config[$connection][$table];
        $options = explode(',', $where);
        foreach ($options as $value) {
            $value = explode(" ", $value);
            if (isset($value[1]) && key_exists($value[0], $databases_config)) {
                $data_column = $databases_config[$value[0]];
                $type = $data_column['type'];
                switch ($type) {
                    case 'int':
                        $value[1] = (int)$value[1];
                        $model = $model->where($data_column['column'], $value[1]);
                        break;
                    case 'double':
                        $value[1] = (double)$value[1];
                        $model = $model->where($data_column['column'], $value[1]);
                        break;
                    case 'string':
                        $value[1] = trim($value[1]);
                        if (in_array($value[1], $data_column['values'])) {
                            $model = $model->where($data_column['column'], $value[1]);
                        }
                        break;
                }
            }

        }
    }
    return $model;
}


function whereInRawQueryBuilder($where, $model, $table_schema, $collection)
{
    $where = explode(' ', $where);
    $field = current($where);
    $values = explode(',', end($where));
    if (isset($table_schema[$field])) {
        $config_field = $table_schema[$field];
        $field_query = $config_field['column'];
        $value_return = [];
        switch ($config_field['type']) {
            case 'int':
                foreach ($values as $value) {
                    $value_return[] = (int)$value;
                }
                break;
            case 'double':
                foreach ($values as $value) {
                    $value_return[] = (double)$value;
                }
                break;
            case 'string':
                $value_check = $config_field['values'];
                foreach ($values as $value) {
                    if (array_search($value, $value_check)) $value_return[] = $value;
                }
                break;
        }
        if (empty($value_return)) return $model;
        return $model->whereIn($field_query, $value_return);
    } else {
        return $model;
    }
}

function whereBetweenRawQueryBuilder($where, $model, $table_schema, $collection)
{
    $where = explode(' ', $where);
    $field = current($where);
    $values = explode(',', end($where));
    if (isset($table_schema[$field])) {
        $config_field = $table_schema[$field];
        $field_query = $config_field['column'];
        $value_return = [];
        switch ($config_field['type']) {
            case 'int':
                foreach ($values as $value) {
                    $value_return[] = (int)$value;
                }
                break;
            case 'double':
                foreach ($values as $value) {
                    $value_return[] = (double)$value;
                }
                break;
            case 'string':
                $value_check = $config_field['values'];
                foreach ($values as $value) {
                    if (array_search($value, $value_check)) $value_return[] = $value;
                }
                break;
            case 'date':
                if (validateDate(current($values)) == true && validateDate(end($values)) == true) {
                    $dateStart = strtotime(current($values));
                    $dateEnd = strtotime(end($values));

                    if ($dateStart == null && $dateEnd == null) {
                        $dateStart = strtotime(date('Y-m-d', time()));
                    }
                    $dateEnd = $dateEnd + 86400;
                    $value_return[] = (int)$dateStart;
                    $value_return[] = (int)$dateEnd;
                }
                break;
        }
        if (empty($value_return)) return $model;
        return $model->whereBetween($field_query, $value_return);
    } else {
        return $model;
    }
}